 // Вопросы по задаче 3, 4, 
 
 //1 Реализуйте класс Worker (Работник), который будет иметь следующие свойства: name (имя), surname (фамилия), rate (ставка за день работы), days (количество отработанных дней). Также класс должен иметь метод getSalary(), который будет выводить зарплату работника. Зарплата - это произведение (умножение) ставки rate на количество отработанных дней days. И метод getFullName() - имя и фамиля работника. 


/* class Worker{
	constructor(name, surname, rate, days){
		this.name = name;
		this.surname = surname;
		this.rate = rate;
		this.days = days;
	}
	getSalary(){
		console.log(this.rate * this.days);
	}
	getFullName(){
		console.log(this.name, this.surname);
	}
} */
/* const worker = new Worker('Иван', 'Иванов', 10, 31);

console.log(worker.name); //выведет 'Иван'
console.log(worker.surname); //выведет 'Иванов'
worker.getFullName(); //выведет 'Иванов Иван'
console.log(worker.rate); //выведет 10
console.log(worker.days); //выведет 31
worker.getSalary(); //выведет 310 - то есть 10*31 */ 

// 2 Напишите новый класс Boss, этот класс наследуется от класса Worker и прошлого задания. Появляется новые свойство: workers - количество работников. И зарплата считается по другому: произведение (умножение) ставки rate на количество отработанных дней и на количество работников.

/* class Boss extends Worker{
	constructor(name, surname, rate, days, workers){
		super(name, surname, rate, days);
		this.workers = workers;
	}
	getSalary(){
		console.log(this.rate * this.days * this.workers);
	}
} */

/* const boss = new Boss('Иван', 'Иванов', 10, 31, 10); */
/* console.log(boss.name); //выведет 'Иван'
console.log(boss.surname); //выведет 'Иванов'
boss.getFullName(); //выведет 'Иванов Иван'
console.log(boss.rate); //выведет 10
console.log(boss.days); //выведет 31
console.log(boss.workers); //выведет 10
boss.getSalary(); //выведет 3100 - то есть 10*31*10 */

// 3 Модифицируйте класс Worker из предыдущей задачи следующим образом: сделайте все его свойства приватными, а для их чтения сделайте методы-геттеры. 

//ВОПРОС!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


/* function WorkerModified(name, surname, rate, days) {
this.getName = function() {
	return name;
}
this.getSurname = function() {
	return surname;
}
this.getRate = function () {
	return rate;
};
this.setRate = function(newRate) {
	rate = newRate;
};
this.getDays = function(){
	return days;
}
this.setDays = function(newDays) {
	days = newDays;
}
this.getSalary = function(){
	return days * rate;
}
}
const workerModified = new WorkerModified('Иван', 'Иванов', 10, 31);
console.log(WorkerModified.getName()); //выведет 'Иван'
console.log(WorkerModified.getSurname()); //выведет 'Иванов'
console.log(WorkerModified.getRate()); //выведет 10
console.log(WorkerModified.getDays()); //выведет 31
console.log(WorkerModified.getSalary()); //выведет 310 - то есть 10*31 */




/* // 4 Модифицируйте класс Worker из предыдущей задачи следующим образом: для свойства rate и для свойства days сделайте еще и методы-сеттеры. 

Пример:
var worker = new Worker('Иван', 'Иванов', 10, 31);

console.log(worker.getRate()); //выведет 10
console.log(worker.getDays()); //выведет 31
console.log(worker.getSalary()); //выведет 310 - то есть 10*31

//Теперь используем сеттер:
worker.setRate(20); //увеличим ставку
worker.setDays(10); //уменьшим дни
console.log(worker.getSalary()); //выведет 200 - то есть 20*10 */

/* // 5 Реализуйте класс MyString, который будет иметь следующие методы: метод reverse(), который параметром принимает строку, а возвращает ее в перевернутом виде, метод ucFirst(), который параметром принимает строку, а возвращает эту же строку, сделав ее первую букву заглавной и метод ucWords, который принимает строку и делает заглавной первую букву каждого слова этой строки.

Пример:
class MyString {
	constructor(){
	}
	reverse(str){
		return str.split('').reverse().join('');
	}
	ucFirst(str){
		return str[0].toUpperCase() + str.slice(1);
	}
	ucWords(str){
		let arr = str.split(' ');
		for(let i = 0; i < arr.length; i++) {
			arr[i] = arr[i][0].toUpperCase() + arr[i].slice(1);
		}
		return arr.join(' ');
	}
}

var str = new MyString();

console.log(str.reverse('abcde')); //выведет 'edcba'
console.log(str.ucFirst('abcde')); //выведет 'Abcde'
console.log(str.ucWords('abcde abcde abcde')); //выведет 'Abcde Abcde Abcde' */

// 6 Реализуйте класс Validator, который будет проверять строки. К примеру, у него будет метод isEmail параметром принимает строку и проверяет, является ли она корректным емейлом или нет. Если является - возвращает true, если не является - то false. Кроме того, класс будет иметь следующие методы: метод isDomain для проверки домена, метод isDate для проверки даты и метод isPhone для проверки телефона.



/* class Validator{
	constructor(){
	}
	isEmail(email){

		let atIndex = email.indexOf('@');
		let lastDotIndex = email.lastIndexOf('.');

	if(atIndex < 0 || email.indexOf('.') < 0){
		return false;
	}
	if(lastDotIndex < atIndex){
		return false;
	}
	if(atIndex < 3){
		return false;
	}
	if(lastDotIndex - atIndex < 4){
		return false;
	}
	if(email.slice(lastDotIndex + 1).length < 2){
		return false;
	}
		return true;
}
	isDomain(url){

		let lastDotIndex = url.lastIndexOf('.');

	if(url.slice(lastDotIndex + 1).length < 2){
		return false;
	}
	if(url.slice(0, lastDotIndex).length < 2){
		return false;
	}

	return true;
}

	isDate(date){

	if(new Date(date) == 'Invalid Date'){
		return false;
	}

	return true;
}

	isPhone(phone){
		
		let phoneNumber = phone.match(/\d/g).join('');

	if(phoneNumber.indexOf('7') != 0 || phoneNumber.length != 11 || phone.indexOf('+') != 0){
		return false;
	}

	return true;
}
};

let validator = new Validator()

console.log(validator.isEmail('phphtml@mail.ru'));
console.log(validator.isDomain('phphtml.net'));
console.log(validator.isDate('12.05.2020'));
console.log(validator.isPhone('79898176892')); */



// 7 Реализуйте класс Student (Студент), который будет наследовать от класса User, подобно тому, как это сделано в теоретической части урока. Этот класс должен иметь следующие свойства: name (имя, наследуется от User), surname (фамилия, наследуется от User), year (год поступления в вуз). Класс должен иметь метод getFullName() (наследуется от User), с помощью которого можно вывести одновременно имя и фамилию студента. Также класс должен иметь метод getCourse(), который будет выводить текущий курс студента (от 1 до 5). Курс вычисляется так: нужно от текущего года отнять год поступления в вуз. Текущий год получите самостоятельно.
//Вот так должен выглядеть класс User, от которого наследуется наш Student:
class User {
	constructor(name, surname) {
		this.name = name;
		this.surname = surname;
	}

	getFullName() {
		return this.name + ' ' + this.surname;
	}
}
