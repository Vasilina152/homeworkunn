/* //1. Реализовать таймер-функцию используя замыкания. Функция принимает два  аргумента начальное значение и значение завершения. Таймер движется назад. Пример: foo(start, end). При достижении точки завершения в консоль выводится значение таймера и сообщение о завершении работы таймера.

timer = setInterval(function () {
  seconds = timeMinut%60 // Получаем секунды
  minutes = timeMinut/60%60 // Получаем минуты
  hour = timeMinut/60/60%60 // Получаем часы
  // Условие если время закончилось то...
  if (timeMinut <= 0) {
      // Таймер удаляется
      clearInterval(timer);
      // Выводит сообщение что время закончилось
      alert("Время закончилось");
  } else { // Иначе
      // Создаём строку с выводом времени
      let strTimer = `${Math.trunc(hour)}:${Math.trunc(minuts)}:${seconds}`;
      // Выводим строку в блок для показа таймера
      timerShow.innerHTML = strTimer;
  }
  --timeMinut; // Уменьшаем таймер
}, 1000); 
console.log(setInterval);
*/

//от преподавателя
function timer (start, end){
  return function (){
    setTimeout(function run(){
        if(start > end){
          console.log(start);
          start--;
          setTimeout(run, 1000)
        } else {
          console.log(start, 'Конец')
        }
    }, 100)
  }
}

const example = timer(10, 3)
example()


/* //2. Что выведет функция? [object Window]
function f() {
  alert( this ); // ?
}

let user = {
  g: f.bind(null)
};

user.g(); */

/* //3. Можем ли мы изменить this дополнительным связыванием?
function f() {
  alert(this.name);
}

f = f.bind( {name: "Вася"} ).bind( {name: "Петя" } );

f();
//Экзотический объект bound function, возвращаемый при первом вызове f.bind(...), запоминает контекст (и аргументы, если они были переданы) только во время создания. Следующий вызов bind будет устанавливать контекст уже для этого объекта. Это ни на что не повлияет. Можно сделать новую привязку, но нельзя изменить существующую. */

/* //4. В свойство функции записано значение. Изменится ли оно после применения bind? Обоснуйте ответ.
function sayHi() {
  alert( this.name );
}
sayHi.test = 5;

let bound = sayHi.bind({
  name: "Вася"
});

alert( bound.test ); // undefined, потому что к bind привязан другой объект, уже нет свойства test. */

/* //5. Вызов askPassword() в приведённом ниже коде должен проверить пароль и затем вызвать user.loginOk/loginFail в зависимости от ответа.
//Однако, его вызов приводит к ошибке. Почему?

//Исправьте выделенную строку, чтобы всё работало (других строк изменять не надо).

function askPassword(ok, fail) {
  let password = prompt("Password?", '');
  if (password == "rockstar") ok();
  else fail();
}

let user = {
  name: 'Вася',

  loginOk() {
    alert(`${this.name} logged in`);
  },

  loginFail() {
    alert(`${this.name} failed to log in`);
  },

};

askPassword(user.loginOk.bind(user), user.loginFail.bind(user));
 */

/* //6. Объект user был изменён. Теперь вместо двух функций loginOk/loginFail у него есть только одна – user.login(true/false).

//Что нужно передать в вызов функции askPassword в коде ниже, чтобы она могла вызывать функцию user.login(true) как ok и функцию user.login(false) как fail?

function askPassword(ok, fail) {
  let password = prompt("Password?", '');
  if (password == "rockstar") ok();
  else fail();
}

let user = {
  name: 'John',

  login(result) {
    alert( this.name + (result ? ' logged in' : ' failed to log in') );
  }
};

askPassword(() => user.login(true), () => user.login(false)); */

/* //8.Напишите в указанном месте конструкцию с методом bind() так, чтобы this внутри функции func всегда указывал на value есть функция const sum = (a, b, c) => a + b + c, которая складывает три числа.из переменной elem. 
var elem = {value: 'Привет'}

function func(surname, name) {
	alert(this.value + ', ' + surname + ' ' + name);
}

let g = f.bind()

func('Иванов', 'Иван'); //тут должно вывести 'привет, Иванов Иван'
func('Петров', 'Петр'); //тут должно вывести 'привет, Петров Петр' */

/* //9. Есть функция const sum = (a, b, c) => a + b + c, которая складывает три числа. Выполните каррирование.
const sum2 = a => b => c => a + b + c

const sum2 = (a) => {
  return (b) => {
    return (c) => {
      return a + b + c;
    };
  };
}; */

/* //10. Напишите функцию которая будет складывать 2 числа. Используя bind вызовите ее  в контексте  другой функции, чтобы эта функция удваивала сумму 2-х элементов.

function sum(a,b){
  return this*(a + b);
}
const doubleSum = sum.bind(2, 4, 5)

console.log(doubleSum());

const tripleSum = sum.bind(3, 4, 5)
console.log(tripleSum()); */