/* function square (a) {
    return a*a;
}

let result = square(2);
alert(result); */






/* function sum (a,b) {
    return a+b;
}

let result = sum(9,7);
alert(result); */






/* function sum (a,b) {
    return a+b;
}

let result = sum(9,7);
alert(result); */






/* function found (a,b,c) {
    return (a+b)/c;
}

let result = found(10,2,3);
alert(result); */




/* let Day = function (day) {
    switch(day) {
        case 1 : return 'Понедельник';
        case 2 : return 'Вторник';
        case 3 : return 'Среда';
        case 4 : return 'Четверг';
        case 5 : return 'Пятница';
        case 6 : return 'Суббота';
        case 7 : return 'Воскресенье';
        default : return 'Некорректное число';
    }
};

alert(Day(17)); */





/* function number (a,b){
    if (a == b){
        return true;
    } else {
        return false;
    }
}
alert(number(1,8)); */





/* function number (a,b){
    if (a + b > 10){
        return true;
    } else {
        return false;
    }
}
alert(number(4,8)); */







/* function number (a){
    if (a >= 0){
        return false;
    } else {
        return true;
    }
}
alert(number(0)); */





/* function isNumberInRange (a){
    if (a > 0 && a < 10){
        return true;
    } else {
        return false;
    }
}
alert(isNumberInRange(0)); */








/* let arr = [-1, -10, 0, 1, 2, 3, 8, 9, 10, 16, 29];
let newArr = [];

for (i = 0; i < arr.length; i++) {
    if(isNumberInRange(arr[i])) {
        newArr.push(arr[i]);
    }
}

function isNumberInRange (a){
    if (a > 0 && a < 10){
        return true;
    } else {
        return false;
    }
}
console.log(newArr); */







/* function getDigitsSum(num) {
    let sum = 0;
    let str = String(num);
    for(let i = 0; i < str.length; i++) {
        sum += Number(str[i]);
    } 
    return sum;
}

let num=+prompt("Укажите число");
console.log(getDigitsSum(num)); */






/* function getDigits(num) {
    return String(num).split("");
}

function getDigitsSum(num) {
    let sum = 0;
    for(let i = 0; i < num.length; i++) {
        sum += +num[i];
    } 
    return sum;
}

function summa(n) {
    return getDigitsSum(getDigits(n));
}

let arrYear = [];
function yearSum(){
    for(let i = 1; i <= 2020; i++) {
        if(summa(i) == 13) {
            arrYear.push(i);
        }
    }
    return arrYear;
}

console.log(yearSum()); */




const lessNine = (number)=> {
    //если переданное число изначально удовлетворяет условию, возвращаем его
    if(number <= 9){
        return number
    } else {
        //переводим число в строку, для того, чтобы была возможночти пройтись в цикле и получить сумму всех чисел
        const strNum = number.toString()
        //переменная для хранения результата
        let result = 0;
        //цикл, в котором получаем сумму всех чисел, из которых состоит передаваемое значение
        for(let i = 0; i<strNum.length; i++){
            // +перед strNum[i] нужен для приведения строчного значения к числу
            result += +strNum[i];
        }
        //рекурсивно вызываем функцию с полученным результатом, пока не попадем в блок if
        return lessNine(result);
    }
}
console.log(lessNine(100));