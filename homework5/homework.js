/* //1. Преобразовать строку в массив слов

//Напишите функцию stringToarray(str), которая преобразует строку в массив слов.


let str = 'Каждый охотник желает знать';
function stringToarray(str) {
    return str.split(" ");
};

//let arr = stringToarray(str);
console.log(stringToarray(str)); // либо использовать строку выше и console.log(arr); */





/* //2. Удаление указанного количества символов из строки

//Напишите функцию delete_characters(str, length), которая возвращает подстроку, состоящую из указанного количества символов.

let str = 'Каждый охотник желает знать';
function delete_characters(str, length) {
    if ((str.constructor === String) && (length > 0)){
        return str.slice(0, length);
    }
};

console.log(delete_characters(str,15)); */


/* //3. Вставить тире между словами строки

//Напишите функцию insert_dash(str), которая принимает строку str в качестве аргумента и вставляет тире (-) между словами. При этом все символы строки необходимо перевести в верхний регистр.

let str = "HTML JavaScript PHP";

function insert_dash(str) {
    return str.toUpperCase().replace(/\s/g, "-") // каждый символ пробела заменяется пустой строкой /g заменяет все вхождения
}
console.log(insert_dash(str)); */




/* //4. Сделать первую букву строки прописной

//Напишите функцию, которая принимает строку в качестве аргумента и преобразует регистр первого символа строки из нижнего регистра в верхний.

let str = "string not starting with capital";  

function cursive_letter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);

}
console.log(cursive_letter(str)); */


/* //5. Первая буква каждого слова заглавная

//Напишите функцию capitalize(str), которая возвращает строку, в которой каждое слово начинается с заглавной буквы.

let str = "каждый охотник желает знать";  

function capitalize(str) {
    return str.replace(/(^|\s)\S/g, function(a) {return a.toUpperCase()}) // берем символы стоящие в начале строки (^) или после пробела (\s) - берем все символы без пробелов(\S)

}
console.log(capitalize(str)); */


/* //6. Смена регистра символов строки
//Напишите функцию change_register(str), которая принимает в качестве аргумента строку и и заменяет регистр каждого символа на противоположный. Например, если вводится «КаЖдЫй ОхОтНиК», то на выходе должно быть «кАжДыЙ оХоТнИк».

let str = "КаЖдЫй ОхОтНиК жЕлАеТ зНаТь";  
let newStr = "";
function change_register(str) {
for (let i=0; i < str.length; i++){
    if(str[i] === str[i].toUpperCase()) {
        newStr += str[i].toLowerCase()
    } else {
        newStr += str[i].toUpperCase()
    }
}
return newStr;
}
console.log(change_register(str)); */



/* //7. Удалить все не буквенно-цифровые символы
//Напишите функцию remove_char(str), которая возвращает строку, очищенную от всех не буквенно-цифровых символов.

let str = "every., -/ hunter #! wishes ;: {} to $ % ^ & * know";  
let strNew = "";

function remove_char(str) {
    for(let i=0; i < str.length; i++){
        if((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z') || str[i] == ' '){ 
            result = strNew += str[i];
        }
    }
    let arr = [];
    for (let i = 0; i <str.split(' ').length; i++){ //не работает с повтором слов
        if(!arr.includes(strNew.split(' ')[i])){
            arr.push(strNew.split(' ')[i])
        }
    }
return arr.join(' ');
};

console.log(remove_char(str)); */

/* let str = "every., -/ hunter #! wishes ;: {} to $ % ^ & * know";  
function remove_char(str) {
    let result = "";
    result = str.replace(/[^\w\s]|_/g, "")
        .replace(/\s+/g, " ");
    return result; 
}
console.log(remove_char(str)); */




/* //8. Нулевое заполнение строки
//Напишите функцию zeros(num, len), которая дополняет нулями до указаной длины числовое значение с дополнительным знаком «+» или « -« в зависимости от передаваемого аргумента.

function zeros (num, len, sign) {
    while((" "+num).length < len) 
    num = "0" + num;
    return num.toString();
}
console.log(zeros(125, 8, '-')); //не проходит с отрицательными */



/* //9. Сравнение строк без учёта регистра
//Напишите функцию comparison(str1, str2), которая сравнивает строки без учёта регистра символов.

function comparison(str1, str2) {
    let result = str1.toLowerCase() === str2.toLowerCase();
    return result;
};
console.log(comparison('spring', 'SPrInG')); */


/* //10. Поиск без учета регистра
//Напишите функцию insensitive_search(str1, str2), которая осуществляет поиск подстроки str2 в строке str1 без учёта регистра символов.

function insensitive_search(str1, str2) {
    let searchStr = new RegExp(str2, "ig");
    var result = str1.search(searchStr);

    return (result > 0) ? "Найдено" : "Не найдено";  
};
console.log(insensitive_search('Я живу в России', 'РоСсИи')); */



/* //11. ВерблюжийРегистр (CamelCase)
//Напишите функцию initCap(str), которая преобразует стиль написания составных слов строки в CamelCase, при котором несколько слов пишутся слитно без пробелов, при этом каждое слово внутри строки пишется с заглавной буквы.

str = "hEllo woRld";
function initCap(str) {
    return str.toLowerCase().replace(/(?:^|\s)[a-z]/g, function(a) {
        return a.toUpperCase().replace(/\s+/g, "");
    });
};
console.log(initCap(str)); */



/* //12. Змеиный_регистр (snake_case)
//Напишите функцию initSnake(str), которая преобразует стиль написания составных слов строки из CamelCase в snake_case, при котором несколько слов разделяются символом подчеркивания (_), причём каждое слово пишется с маленькой буквы.

function initSnake(str) {
    let str = str.replace(/[A-Z]/g, 
    
    function (letter) {
    return '_' + letter.toLowerCase();
    });
    return str.replace(/^_/, "");
}

console.log(initSnake('HelloWorld')); */




/* //13. Повторить строку n раз
//Напишите функцию repeatStr(str, n), которая вовращает строку повторяемую определённое количество раз.

function repeatStr(str, n) { 
    let strNew = '';
    while (n-- > 0) strNew += str;
    return strNew;
};
console.log(repeatStr('Spring!!!', 3)); */



/* //14. Получить имя файла
//Напишите функцию path(pathname), которая вовращает имя файла (подстрока после последнего символа "\" ) из полного пути к файлу.

let pathname = "/home/user/dir/file.txt";

function path(pathname) { 
    let name= pathname.split('\\').pop().split('/').pop();
    return name;
};
console.log(path(pathname)); */



/* //15. Заканчивается ли строка символами другой строки
//Создайте метод объекта String endsWith(), который сравнивает подстроку str1 с окончанием исходной строки str и определяет заканчивается ли строка символами подстроки.

let str = "Каждый охотник желает знать"; 
let str1 = "скрипт";
let str2 = "знать";

String.prototype.endsWith = function(substring) {
    if(substring.length > this.length) return false;
    return this.substr(this.length - substring.length) === substring;
};

console.log(str.endsWith(str1));
console.log(str.endsWith(str2)); */



/* //16. Подстрока до/после указанного символа
//Напишите функцию getSubstr(str, char, pos), которая возвращает часть строки, расположенную после или до указанного символа char в зависимости от параметра pos.

let str = 'Астрономия — Наука о небесных телах';

function getSubstr(str, char, pos) {
    if(pos == 'after'){
    return str.slice(str.indexOf(char) + 1);
    } else if(pos == 'before'){ // до
    return str.slice(0, str.indexOf(char));
    } else {
    return str;  
    };
};
console.log(getSubstr(str, 'я','before'));
console.log(getSubstr(str, 'н','after')); */



/* //17. Вставить подстроку в указанную позицию строки
//Напишите функцию insert(str, substr, pos), которая вставляет подстроку substr в указханную позицию pos строки str. По умолчанию подстрока вставляется в начало строки.

function insert(str, substr, pos) {
    let array = str.split('');
    array.splice(pos, 0, substr);
    return array.join('');
};

console.log(insert('Уйти нельзя', 'Остаться '));
console.log(insert('Уйти нельзя', 'Остаться ', 5)); */


/* //18. Ограничить длину строки
//Напишите функцию limitStr(str, n, symb), которая обрезает строку, если она длиннее указанного количества символов n. Усеченная строка должна заканчиваться троеточием «...» (если не задан параметр symb) или заданным символом symb.

function limitStr(str, n, symb) {
    if (!n && !symb) return str;
    symb = symb || '...';
    return str.substr(0, n - symb.length) + symb;
};
console.log(limitStr('Звездочка в небе дрожит, кто тебя бросил в мокрую осень', 24, '!')); */



/* //19. Поделить строку на фрагменты
function cutString(str, n) {
    let part = [];
    for(let i = 0 ; i < str.length; i += n) {
    part.push(str.substr(i, n));
    }
    return part;
};
console.log(cutString('превысокомногорассмотрительствующий', 2)); */



//20. Количество вхождений символа в строке
//Напишите функцию count(str, stringsearch), которая возвращает количество символов stringsearch в строке str.








