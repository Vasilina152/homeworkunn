//Работа с if-else

/* const a = prompt("Чему равна переменная а?"); */

/* const a = 2; */
/* if (a == 0) {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */


/* if (a > 0) {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */


/* if (a < 0) {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */

/* if (a <= 0) {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */


/* if (a >= 0) {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */

/* if (a != 0) {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */

/* if (a == "test") {
    alert( 'Верно!' );
    } else {
    alert( 'Неверно' );
    } */


/* if (a === "1") {
alert( 'Верно!' );
} else {
alert( 'Неверно' );
} */





//Работа с логическими переменными

/* const test= 'false';
if (test == 'true') {
    alert('Верно!');
} else {
    alert('Неверно!');
}

const test ="";
if (test)
    alert('Верно!');
alert('Неверно!'); */

/* const test= 'true';
if (test == 'true') {
    alert('Верно!');
} else {
    alert('Неверно!');
}

const test ="2";
if (test)
    alert('Верно!');
alert('Неверно!'); */



/* const test = 'false';
if (test != 'true') {
    alert('Верно!');
} else {
    alert('Неверно!');
}

const test ="";
if (!test)
    alert('Верно!');
alert('Неверно!'); */



/* const test= 'true';
if (test != 'true') {
    alert('Верно!');
} else {
    alert('Неверно!');
}

const test ="2";
if (!test)
    alert('Верно!');
alert('Неверно!'); */




//Работа с && (и) и || (или)


/* const a = 0; */
/* if (a > 0 && a < 5) {
    alert('Верно');
} else {
    alert('Неверно');
} */



/* let a = 2;

if (a == 0 || a == 2) {
    a += 7;
} else {
    a /= 10;
}
console.log(a); */



/* let a = 3;
let b = 5;

if (a <= 1 && b >= 3) {
    c = a + b;
} else {
    c = a - b;
}
console.log(c); */


/* let a = 0;
let b = 0;

if ((a > 2 && a < 11) || (b >= 6 && b < 14)) {
    alert('Верно'); 
} else {
    alert('Неверно');
} */


/* num = 3;
result = null;

switch(num) {
    case 1: result = 'Зима';
    break;
    case 2: result = 'Весна';
    break;
    case 3: result = 'Лето';
    break;
    case 4: result = 'Осень';
    break;
    default:
    alert( "Нет таких значений" );
}
console.log(result); */


/* const day = prompt('Определить декаду дня');

if (day <= 10) {
    alert('Декада 1');
} else if (day >= 11 && day <= 20) {
    alert('Декада 2');
} else (day >=21 && day <= 31) {
    alert('Декада 3');
} */





//Общие задачи


/* const day = prompt("Определить декаду");

if (day <= 10) {
    alert( 'Декада 1' );
    } else if (day >= 11 && day <= 20) {
        alert( 'Декада 2' );
    } else if (day >=21 && day <= 31) {
        alert( 'Декада 3' );
    } else {
        alert('Неопределено');
    } */


/* const mounth = prompt("Определить время года по месяцу");

if (mounth <= 2 || mounth == 12) {
    alert( 'Зима' );
    } else if (mounth >= 3 && mounth <= 5) {
        alert( 'Весна' );
    } else if (mounth >=6 && mounth <= 8) {
        alert( 'Лето' );
    } else if (mounth >=9 && mounth <= 11) {
        alert( 'Осень' );
    } else {
        alert('Неопределено');
    } */



/* let string = 'dbcae';

if (string[0] === 'a') {
    alert('Yes');
} else {
    alert('No');
} */



/* let string = '51345';

if (string[0] == '1' || string[0] == '2' || string[0] == '3') {
    alert('Yes');
} else {
    alert('No');
} */



/* let string = '193';
let sum = Number(string[0])+Number(string[1])+Number(string[2]);
console.log(sum); */



/* let string = '193391';
let sum1 = Number(string[0])+Number(string[1])+Number(string[2]);
let sum2 = Number(string[3])+Number(string[4])+Number(string[5]);

if (sum1 === sum2 ) {
    alert('Yes');
} else {
    alert('No');
} */






//Циклы while и for


/* let a=0;
while (a <= 100 ){
    console.log("======>", a);
    a++;
}

for(let a=1; a <= 100; a+=1) {
    console.log("======>", a);
    
} */




/* let a = 11;
while (a <= 33 ){
    console.log("======>",a);
    a++;
}

for(let a=11; a <= 33; a++) {
    console.log("======>", a);
    
} */


/* let a=0;
while (a <= 100 ){
    console.log("======>", a);
    a+=2;
}

for(let a=0; a <= 100; a+=2) {
    console.log("======>", a);
    
} */



/* let sum = 0
for(let a=1; a <= 100; a++) {
    sum += a;
}
console.log(sum); */





//Работа с for для массивов


/* let arr = [1, 2, 3, 4, 5]

for(let a = 0; a < arr.length; a++){
    console.log(arr[a]);
}


let arr = [1, 2, 3, 4, 5]
let result = 0;

for(let a = 0; a < arr.length; a++){
    result += arr[a];
}
console.log(result); */






//Задачи общие.



/* let arr = [2, 5, 9, 15, 0, 4];

for(let a = 0; a < arr.length; a+=1){
    if (arr[a] > 3 && arr[a] < 10) {
    console.log(arr[a]);
}
} */



/* let arr = [-2, 5, 9, -15, 0, 4];
let sum = 0

for(let a = 0; a < arr.length; a+=1){
    if (arr[a] >= 0) {
    sum += arr[a];
}
}
console.log(sum); */



/* let arr = [1, 2, 5, 9, 4, 13, 4, 10];

for(let a = 0; a < arr.length; a+=1){
    if(arr[a] == '4'){
        alert('Есть!')
    }
}
console.log(arr[a]); */



/* let arr = [10, 20, 30, 50, 235, 3000];
let result = 0;
const chrs = ['1', '2', '5'];

for(let a = 0; a < arr.length; a+=1){
    result = arr.filter(a => chrs.includes(a.toString()[0]))
}
console.log(result); */



/* let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let result = "";

for(let a = 0; a < arr.length; a+=1){
    result += '-' + arr[a];
}
console.log(result); */




/* let day = ['Monday', 'Ttuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    for (name in day){
        if (day[name] === 'Saturday' || day[name] === 'Sunday') {
        document.write('<b>'+day[name]+'</b><br>');
    } else {
        document.write(day[name]+'<br>');
    }
} */



/* let week = ['Monday', 'Ttuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
let day = 'Friday';
    for(let a = 0; a < week.length; a++){
        if( week[a] == day) {
        document.write('<i>'+ week[a] + '</i><br>');
    } else {
        document.write(week[a] + '<br>');
        }
} */
