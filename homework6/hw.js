/* //1.Используя метод map() напишите код, который получает из массива строк новый массив, содержащий их длины.

let vegetables = ["Капуста", "Репа", "Редиска", "Морковка"];
let vegetableLength = vegetables.map(item => item.length);

console.log( vegetableLength ); */

/* //2.Имеется массив простых чисел: numbers = [2, 3, 5, 7, 11, 13, 17, 19]. Использую метод reduce() напишите функцию currentSums(numbers), которая возвращает новый массив из такого же числа элементов, в котором на каждой позиции будет находиться сумма элементов массива numbers до этой позиции включительно. для 

numbers = [2, 3, 5, 7, 11, 13, 17]

function currentSums(numbers) {
    let result = [];
    numbers.reduce(function(sum, current, i) {
        return result[i] = sum + current;
    }, 0);
    return result;
}
console.log(currentSums(numbers));  */



/* //3.Напишите код, который получает из массива чисел новый массив, содержащий пары чисел, которые в сумме должны быть равны семи: (0:7), (1:6) и т.д.

let arr = [0, 1, 2, 3, 4, 5, 6, 7];
function sumSeven(numbers) {
    let object = {};
    let result = [];
    
    for(let i = 0; i < numbers.length; i++) {
    let elem = numbers[i];
    
    for(let j = i + 1; j < numbers.length; j++) {
        let item = numbers[j];
    
    if(elem + item != 7) {
        continue;    
        }
    
        let num = elem + ':' + item;
        object[num] = num;
    }
    }

    for(let key in object) {
    result.push(object[key]);
    }
    return result;
    }

console.log(sumSeven(numbers)); */



/* //4.Перед вами переменная, содержащая строку. Напишите код, создащий массив, который будет состоять из первых букв слов строки str. 

let str = "Каждый охотник желает знать, где сидит фазан."; 

function firstChar(value, index, arr) {  
    if (index == 0)  {
        return true; 
    } else {
        return arr[index - 1] === " ";  
    }     
}  
let array = [].filter.call(str, firstChar);  

console.log(array);  */





/* //5.Перед вами переменная, содержащая строку. Напишите код, создащий массив, который будет состоять из строк, состоящих из предыдущего, текущего и следующего символа строки str. 


let str = "JavaScript"; 

function getChars(value, index, str) {
    return str.substring(index - 1, index + 2);
    }
    let arr = Array.prototype.map.call(str, getChars);

console.log(arr); */

/* //6.Напишите код, преобразующий массив цифр, которые располагаются неупорядоченно, в массив цифр расположенных по убыванию их значений.

let numerics = [5, 7, 2, 9, 3, 1, 8, 0];
numerics.sort(function(a,b){return a*b==0? 1:a-b});
console.log(numerics); */


/* //7.Напишите код, объединяющий три массива цифр, и располагающий цифры, в полученном массиве, в порядке убывания их значений через пробел.

let a = [1,2,3];
let b = [4,5,6];
c = [7,8,9];

let d = a.concat(b,c);
d.sort((x, y) => (y - x));

console.log(d); */

/* //8.Дан двухмерный массив с числами, например [[1, 2, 3], [4, 5], [6]]. Найдите сумму элементов этого массива. Массив, конечно же, может быть произвольным. Показать решение.
let arr = [[1, 2, 3], [4, 5], [6]];
let sum = 0;
for (let i = 0; i < arr.length; i++) {
	for (let j = 0; j < arr[i].length; j++) {
		sum += arr[i][j];
	}
}
console.log(sum); */


/* //9.Дан трехмерный массив с числами, например [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]. Найдите сумму элементов этого массива. Массив, конечно же, может быть произвольным.

let arr = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]];
let sum = 0;
for (let i = 0; i < arr.length; i++) {
	for (let j = 0; j < arr[i].length; j++) {
        for (let k = 0; k < arr[i][j].length; k++) {
	        sum += arr[i][j][k];
}
}
}
console.log(sum); */

/* //10.Дан массив с числами. Не используя метода reverse переверните его элементы в обратном порядке.

let arr = [2, 5, 0, 12, 8, -3];
for(let i = 0, j = arr.length - 1; i < j; i++, j--)
    [arr[i], arr[j]] = [arr[j], arr[i]];
console.log(arr);  */


//11.Дан массив с числами. Узнайте сколько элементов с начала массива надо сложить, чтобы в сумме получилось больше 10-ти.

/* let arr = [10, 2, 3, 0, 4, 5, 6];
console.log(arr.reduce((p, c) => p[1] < 10 ? [p[0] + 1, p[1] + c] : p, [0, 0])[0]); //В качестве накопителя используется массив с двумя значениями: первое - количество элементов, добавленных к сумме; второе - собственно сама сумма. Логика функции, переданной редьюсу предельно проста: проверяется сумма (второй элемент массива), если она меньше десяти, возвращаем массив, в котором первый элемент увеличивается на единицу, второй - на значение текущего элемента массива, в случае если сумма уже превысила десять - возвращаем накопитель без изменений. В результате получаем массив, в котором нас интересует первый элемент. */


/* let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
function func(arr){
let sum = 0;
for (let i = 0; i < arr.length; i++) {
    sum += arr[i];
    if (sum > 10) {
    return i + 1;
} 
}
}
console.log(func(arr)); */





//12.Сделайте функцию arrayFill, которая будет заполнять массив заданными значениями. Первым параметром функция принимает значение, которым заполнять массив, а вторым - сколько элементов должно быть в массиве. Пример: arrayFill('x', 5) сделает массив ['x', 'x', 'x', 'x', 'x'].

function arrayFill(value, length) { 
	let arr = [];
	for (let i = 0; i < length; i++) {
		arr.push(value);
	}
	return arr;
}

console.log(arrayFill('x', 5));


